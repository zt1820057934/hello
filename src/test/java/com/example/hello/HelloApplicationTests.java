package com.example.hello;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HelloApplicationTests {

    @Test
    void contextLoads() {
        System.out.println("------------------------------------------------");
        System.out.println("------------Hello world Test--------------------");
        System.out.println("------------------------------------------------");
    }

}
